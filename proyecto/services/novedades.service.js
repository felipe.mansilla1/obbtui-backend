'use strict';
var reply = require('../../base/utils/reply');

function getNovedades(request, response) {
  try {
    var novedades = [];

    // Obtener las novedades
    let resp = {
      id: 1,
      titulo: 'Bienvenidos',
      cuerpo: 'Bienvenido(a) a la nueva aplicación AppTUI de la Universidad de Valparaíso.' +
      ' Podrás revisar información como tus asignaturas inscritas, horarios, saldos, etc. ',
      imagen: 'https://i.imgur.com/EZDqCq3.png',
      fecha: '2018-01-15 17:16:50.090',
      autor: 'Universidad de Valparaíso'
    };

    novedades.push(resp);

    response.json(reply.ok(novedades));
  } catch(err) {
    response.json(reply.fatal(err));
  }
}

module.exports = {
  getNovedades
};